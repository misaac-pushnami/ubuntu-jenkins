# Ubuntu 18.04 + Jenkins Server

## Virtual Machine
- 1 vCPU
- 1024 MB RAM
- 25 GB Storage

## Setup Steps
1. Run `curl {{setup.sh raw URL}} | /bin/bash`
2. When prompted, navigate to {{URL || IP address}}:8080 and setup Jenkins