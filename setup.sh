#! /bin/bash
# Author: Marvin Isaac <misaac@pushnami.com>
set -e

wait () {
    sleep 2
}

update_system () {
    printf "> Getting updates...\n"
    wait
    sudo apt update -y

    printf "> Installing updates...\n"
    wait
    sudo apt upgrade -y

    printf ">>> Updates installed.\n"
    wait
}

install_packages () {
    printf "> Installing Java 8 JRE...\n"
    wait
    sudo apt install openjdk-8-jre -y

    java -version
    printf ">>> Java 8 JRE installed.\n"
    wait
    
    # Source: https://docs.docker.com/install/linux/docker-ce/ubuntu/
    printf "> Installing Docker...\n"
    wait
    sudo apt-get remove docker docker-engine docker.io containerd runc -qq
    sudo apt-get update -qq
    sudo apt-get install \
        apt-transport-https \
        ca-certificates \
        curl \
        gnupg-agent \
        software-properties-common \
        -qq
    curl -fSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
    sudo add-apt-repository \
        "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
        $(lsb_release -cs) \
        stable"
    sudo apt-get update -qq
    sudo apt-get install docker-ce docker-ce-cli containerd.io -qq
    
    sudo docker run hello-world
    printf ">>> Docker installed.\n"
    wait

    # Source: https://jenkins.io/doc/pipeline/tour/getting-started/
    printf "> Installing Jenkins...\n"
    wait
    sudo mkdir /var/www/html/jenkins/ -p
    sudo curl -LS http://mirrors.jenkins.io/war-stable/latest/jenkins.war \
        -o /var/www/html/jenkins/jenkins.war
    cd /var/www/html/jenkins
    printf "> Running Jenkins on port 8080...\n"
    java -jar jenkins.war --httpPort=8080
}

identifier="local-ubuntu-jenkins-server"
user=$(whoami)

update_system
install_packages